package iferr

import (
	"log"
)

func Panic(err error) {
	if err != nil {
		panic(err)
	}
}

func Exit(err error, msg string) {
	if err != nil {
		if msg != "" {
			log.Fatalf("%v, %s", err, msg)
		} else {
			log.Fatal(err)
		}
	}
}
