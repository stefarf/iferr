package iferr

import (
	"fmt"
	"log"
)

func Fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func FatalMsg(err error, msg string) {
	if err != nil {
		log.Fatal(fmt.Sprintf("%s: %v", msg, err))
	}
}

func Panic(err error) {
	if err != nil {
		panic(err)
	}
}

func Print(err error) {
	if err != nil {
		log.Println(err)
	}
}

func Println(err error) {
	if err != nil {
		log.Println(err)
	}
}

func PrintMsg(err error, msg string) {
	if err != nil {
		log.Printf("%s: %v", msg, err)
	}
}
